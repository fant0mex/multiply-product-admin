#pylint: disable=no-self-use, invalid-name

import endpoints
import datetime
import json

from protorpc import messages
from protorpc import message_types
from protorpc import remote

from models import SavingsAccount, Provider

from google.appengine.ext import ndb
from google.appengine.api import users

from config import USERS
from settings import WEB_CLIENT_ID

def get_user_type(email):
    for u_type, u_list in USERS.iteritems():
        if email in u_list:
            return u_type
    return None

def userAuth(func):
    def func_wrapper(self, query):
        user = users.get_current_user()
        if user is None:
            print 'user not found'
            message = 'user not found'
            raise endpoints.NotFoundException(message)
        email = user.email()
        user_type = get_user_type(email)
        if user_type is None:
            message = '"%s" is not an authed user' % email
            raise endpoints.UnauthorizedException(message)
        return func(self, query)
    return func_wrapper

@endpoints.api(
    name='productendpoints',
    version='v1',
    description='Multiply Products API'
)

class ProviderApi(remote.Service):
    """Provider API v1."""

    """Provider Routes"""
    @Provider.query_method(
        path='getProviders',
        name='getProviders',
        http_method='GET',
        query_fields=('perPage', 'page', 'order', 'id_list')
    )
    
    @userAuth
    def getProviders(self, query):
        return query

    @Provider.method(
        path='getProviders/{id}',
        name='getProvider',
        http_method='GET'
    )

    def getProvider(self, provider):
        return provider

    @Provider.method(
        path='getProviders',
        name='addProvider',
        http_method='POST'
    )

    def addProvider(self, provider):
        provider.put()
        return provider

    @Provider.method(
        path='getProviders/{id}',
        name='updateProvider',
        http_method='PUT'
    )

    def updateProvider(self, provider):
        provider.put()
        return provider

    @Provider.method(
        path='getProviders/{id}',
        name='deleteProvider',
        http_method='DELETE'
    )

    def deleteProvider(self, provider):
        provider.key.delete()
        return provider

    """Savings Accounts Routes"""
    @SavingsAccount.query_method(
        path='savingsAccounts',
        name='getSavingsAccounts',
        http_method='GET',
        query_fields=('perPage', 'page', 'order')
    )

    def getSavingsAccounts(self, query):
        return query

APPLICATION = endpoints.api_server([ProviderApi])