import os
import google
from google.appengine.ext import vendor

# SET FOLDER FOR EXTERNAL MODULES
lib_directory = os.path.dirname(__file__) + '/lib'

# MANUALLY SET GOOGLE MODULE PATH, OTHERWISE IT LOOKS IN LOCAL GOOGLE CLOUD FOLDER
# https://github.com/GoogleCloudPlatform/google-auth-library-python/issues/169
google.__path__ = [os.path.join(lib_directory, 'google')] + google.__path__

# ADD ANY LIBRARIES IN THE LIB FOLDER
vendor.add(lib_directory)
