import React, { Component } from 'react';
import {
  BooleanField,
  BooleanInput,
  Create,
  Edit,
  List,
  Datagrid,
  DateField,
  TextField,
  TextInput,
  required,
  ReferenceField,
  ReferenceInput,
  SelectInput,
  SimpleForm,
  FunctionField,
  NumberField,
  NumberInput,
  CheckboxGroupInput,
} from 'admin-on-rest';

import { isUndefined } from 'lodash'

const choices = [
  {id: 'branch', name: 'Branch'},
  {id: 'online', name: 'Online'},
  {id: 'phone', name: 'Phone'},
]

class RateCalcField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rate: 0
    }
  }
  
  handleChange = (record) => (e) => {
    const { value } = this.input;
    const { rtb } = record;
    
    let interest = 0;

    rtb.forEach((bin) => {
      if (isUndefined(bin.balance.min) || value > bin.balance.min) {
        const upper = isUndefined(bin.balance.max) ? value : Math.min(value, bin.balance.max);
        const lower = isUndefined(bin.balance.min) ? 0 : bin.balance.min;
        const total = upper - lower;
        interest += ((total * bin.aer) / value);
      }
    });

    const rate = interest > 0 ? parseFloat(interest).toFixed(3) : 0;
    
    this.setState({
      rate
    });
  }
  
  render() {
    const { record, source } = this.props;
    const { rate } = this.state;
    return (
      <div style={{display: 'flex', alignItems: 'baseline'}}>
        <input
          placeholder="Initial Amount"
          type="number"
          ref={node => this.input = node}
          onChange={this.handleChange(record)} />
        <div>{rate > 0 && rate}</div>
      </div>
    )
  }
}

export const AccountsList = (props) => (
  <List title="Savings Accounts" {...props}>
    <Datagrid>
      <TextField source="name" />
      <ReferenceField label="Provider" source="provider_id" reference="getProviders" sortable={false}>
        <TextField source="name" />
      </ReferenceField>
      <FunctionField source="accessibility" label="Accessibility" render={record => record.accessibility !== undefined ? record.accessibility.map(a => `${a}, `) : ''} />
      <BooleanField source="additions_allowed" label="Additions" />
      <DateField source="created" />
      <BooleanField source="early_access_allowed" label="Early Access" />
      <BooleanField source="existing_members_only" />
      <RateCalcField label="Rate Calculator" />
      <FunctionField source="headline_rate_1000" label="headline_rate_1000" render={record => <span style={{color: record.headline_rate_1000 > 2 ? 'red' : 'inherit'}}>{parseFloat(record.headline_rate_1000).toFixed(3)}</span>} />
      <NumberField source="initial_deposit" options={{ style: 'currency', currency: 'GBP' }} />
      <TextField source="interest_freq" label="Interest Frequency" />
      <TextField source="isa_type" />
      <TextField source="age" label="Minimum Age" />
      <NumberField source="balance" label="Minimum Balance" options={{ style: 'currency', currency: 'GBP' }} />
      <DateField source="updated" />
      <BooleanField source="withdrawals_allowed" />
    </Datagrid>
  </List>
);

export const AccountCreate = (props) => (
  <Create {...props} title="Create Savings Account">
    <SimpleForm>
      <TextInput source="name" validate={required}/>
      <ReferenceInput
        label="Provider"
        source="provider"
        reference="getProviders"
        sort={{ field: 'name', order: 'ASC' }}
        perPage={150}
        allowEmpty>
        <SelectInput
          optionText="name"
        />
      </ReferenceInput>
      <CheckboxGroupInput source="accessibility" choices={choices} />
      <BooleanInput source="additions_allowed" label="Additions" />
      <BooleanInput source="early_access_allowed" label="Early Access" />
      <BooleanInput source="existing_members_only" />
      <BooleanInput source="withdrawals_allowed" />
      <NumberInput source="initial_deposit" format={v => parseFloat(v).toFixed(2)} parse={v => parseFloat(v)} />
      <NumberInput source="balance" label="Minimum Balance" format={v => parseFloat(v).toFixed(2)} parse={v => parseFloat(v)}/>
      <TextInput source="interest_freq" label="Interest Frequency" />
      <TextInput source="isa_type" />
      <NumberInput source="age" label="Minimum Age" step={1} />
    </SimpleForm>
  </Create>
);

const ProductTitle = ({ record }) => {
  return <span>{record ? `${record.name}` : ''}</span>;
};

export const AccountEdit = (props) => (
  <Edit title={<ProductTitle />} {...props}>
    <SimpleForm>      
      <TextInput source="name" />
      <ReferenceInput
        label="Provider"
        source="provider"
        reference="getProviders"
        sort={{ field: 'name', order: 'ASC' }}
        perPage={150}
        allowEmpty
      >
        <SelectInput
          optionText="name"
        />
      </ReferenceInput>
      <CheckboxGroupInput source="accessibility" choices={choices} />
      <BooleanInput source="additions_allowed" label="Additions" />
      <BooleanInput source="early_access_allowed" label="Early Access" />
      <BooleanInput source="existing_members_only" />
      <BooleanInput source="withdrawals_allowed" />
      <NumberInput source="initial_deposit" format={v => parseFloat(v).toFixed(2)} parse={v => parseFloat(v)} />
      <NumberInput source="balance" label="Minimum Balance" format={v => parseFloat(v).toFixed(2)} parse={v => parseFloat(v)} />
      <TextInput source="interest_freq" label="Interest Frequency" />
      <TextInput source="isa_type" />
      <NumberInput source="age" label="Minimum Age" step={1} />
    </SimpleForm>
  </Edit>
);
