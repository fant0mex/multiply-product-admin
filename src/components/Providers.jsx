import React from 'react';
import {
  List,
  Edit,
  Create,
  Datagrid,
  DateField,
  ImageField,
  TextField,
  EditButton,
  DisabledInput,
  LongTextInput,
  required,
  SimpleForm,
  TextInput,
  Filter,
  Responsive,
  SimpleList,
} from 'admin-on-rest';

const ProviderFilter = (props) => (
  <Filter {...props}>
    <TextInput label="Search" source="q" alwaysOn />
  </Filter>
);

export const ProviderList = (props) => (
  <List {...props} title="Providers" filters={<ProviderFilter/>}>
    <Responsive
      small={
        <SimpleList
            primaryText={record => record.title}
            secondaryText={record => `${record.views} views`}
            tertiaryText={record => new Date(record.published_at).toLocaleDateString()}
        />
      }
      medium={
        <Datagrid>
            <TextField source="name" />
            <ImageField source="image_url" />
            <DateField source="created" />
            <DateField source="updated" />
            <EditButton />
        </Datagrid>
      }
    />
  </List>
);

const PostTitle = ({ record }) => {
  return <span>{record ? `${record.name}` : ''}</span>;
};

export const ProviderEdit = (props) => (
  <Edit title={<PostTitle />} {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="name" />
      <TextInput source="image_url" />
      <LongTextInput source="description" />
    </SimpleForm>
  </Edit>
);

export const ProviderCreate = (props) => (
  <Create {...props} title="Create Provider">
    <SimpleForm>
      <TextInput source="name" validate={required}/>
      <TextInput  label="id" source="provider_id" validate={required}/>
      <LongTextInput source="description" />
      <TextInput source="image_url" />
      <TextInput source="website" />
    </SimpleForm>
  </Create>
);