import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_ERROR, AUTH_CHECK } from 'admin-on-rest';

export default (type, params) => {

  if (type === AUTH_LOGIN) {
    const { username } = params;
    fetch(`${process.env.PRODUCT_API}/user`)
      .then((response) => {
        localStorage.setItem('signedIn', true);
        return Promise.resolve()
    });
  }

  if (type === AUTH_LOGOUT) {
    localStorage.removeItem('signedIn');
    return Promise.resolve();
  }

  if (type === AUTH_ERROR) {
    const { status } = params;
    if (status === 401 || status === 403) {
      localStorage.removeItem('username');
      return Promise.reject();
    }
    return Promise.resolve();
  }

  if (type === AUTH_CHECK) {
    return localStorage.getItem('signedIn') ? Promise.resolve() : Promise.reject();
  }
};
