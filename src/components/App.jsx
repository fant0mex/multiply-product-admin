import React from 'react';
import { Admin,  Delete, Resource } from 'admin-on-rest';
import { ProviderList, ProviderEdit, ProviderCreate } from './Providers';
import { AccountsList } from './SavingsAccounts';
import AuthClient from './AuthClient';
import { restClient } from './RestClient';
import PostIcon from 'material-ui/svg-icons/action/book';
import EditorAttachMoney from 'material-ui/svg-icons/editor/attach-money';

const App = () => (
  <Admin
    title="Multiply Product DB"
    restClient={restClient(process.env.PRODUCT_API)}
  >
    <Resource
      name="getProviders"
      options={{ label: "Providers" }}
      list={ProviderList}
      edit={ProviderEdit}
      create={ProviderCreate}
      remove={Delete}
      icon={PostIcon}
    />
    <Resource
      name="savingsAccounts"
      options={{ label: "Savings Accounts" }}
      list={AccountsList}
      icon={EditorAttachMoney}
    />
  </Admin>
)

export default App;
