from google.appengine.ext import ndb
from protorpc import messages
from protorpc import message_types

from endpoints_proto_datastore import EndpointsModel
from endpoints_proto_datastore.properties import EndpointsComputedProperty, EndpointsAliasProperty

from config import CONSTANTS

class Provider(EndpointsModel):
    name = ndb.StringProperty()
    provider_id = ndb.StringProperty()
    description = ndb.TextProperty()
    image_url = ndb.StringProperty()
    website = ndb.StringProperty()
    updated = ndb.DateTimeProperty(auto_now=True)
    created = ndb.DateTimeProperty(auto_now_add=True)


class AmountConstraint(EndpointsModel):
    min = ndb.FloatProperty()
    max = ndb.FloatProperty()

    def check(self, value):
        if self.min is not None and value < self.min:
            return False
        if self.max is not None and value > self.max:
            return False
        return True

class IntegerConstraint(AmountConstraint):
    min = ndb.IntegerProperty()
    max = ndb.IntegerProperty()

class InterestRateBalanceBin(EndpointsModel):
    aer = ndb.FloatProperty()
    interest_type = ndb.StringProperty(choices=["fixed", "variable"])
    balance = ndb.StructuredProperty(AmountConstraint)

class RateBin(messages.Message):
    aer = messages.FloatField(1)
    interest_type = messages.StringField(2)
    balance_min = messages.FloatField(3)
    balance_max = messages.FloatField(4)

def calculate_effective_rate(amount):
    def calculate(model):
        weighted_interest = 0
        for r_bin in model.rate_bins:
            if r_bin.balance.min is None or amount > r_bin.balance.min:
                upper = min(amount, r_bin.balance.max) if r_bin.balance.max is not None else amount
                lower = r_bin.balance.min if r_bin.balance.min is not None else 0
                total_in_bin = upper - lower
                weighted_interest += total_in_bin * r_bin.aer
        return str(weighted_interest / amount)
    return calculate

class SavingsAccount(EndpointsModel):
    description = ndb.TextProperty()
    comments = ndb.TextProperty()
    isa_type = ndb.StringProperty(choices=[opt for opt in CONSTANTS["isa_types"]], default="none")
    headline_rate_1000 = EndpointsComputedProperty(calculate_effective_rate(1000))
    rate_bins = ndb.LocalStructuredProperty(InterestRateBalanceBin, "rtb", repeated=True)
    interest_freq = ndb.StringProperty() 
    balance = ndb.FloatProperty()
    initial_deposit = ndb.FloatProperty()
    age = ndb.IntegerProperty()
    existing_members_only = ndb.BooleanProperty(default=False)
    conditions = ndb.TextProperty("cond")
    instant_access = ndb.BooleanProperty(default=False)
    term_date_days = ndb.IntegerProperty()
    isa_transfer_in = ndb.BooleanProperty(default=False)
    additions_allowed = ndb.BooleanProperty(default=False)
    withdrawals_allowed = ndb.BooleanProperty(default=False)
    early_access_allowed = ndb.BooleanProperty(default=False)
    accessibility_opening = ndb.StringProperty("ac_op", repeated=True)
    accessibility = ndb.StringProperty(repeated=True)
    restrictions = ndb.TextProperty("rest")
    updated = ndb.DateTimeProperty(auto_now=True)
    created = ndb.DateTimeProperty(auto_now_add=True)
    live = ndb.BooleanProperty(default=False)
    name = ndb.StringProperty()
    provider = ndb.KeyProperty(kind=Provider)
    datasource = ndb.StringProperty()
    datasource_id = ndb.StringProperty()
    execution_link = ndb.StringProperty()
    flags = ndb.StringProperty(repeated=True)

    @EndpointsAliasProperty()
    def provider_id(self):
       return str(self.provider.id())
