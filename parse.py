from models import Provider, SavingsAccount, AmountConstraint, IntegerConstraint, InterestRateBalanceBin

# load json file

def get_provider(name, image_url, providers=None):
    if providers is None:
        providers = {}
    provider_id = name.lower().strip()
    if provider_id in providers:
        provider = Provider.get_by_id(providers[provider_id])
    else:
        provider = Provider.query(Provider.provider_id == provider_id).get()
        if not provider:
            provider = Provider(name=name, provider_id=provider_id, image_url=image_url)
            provider.put()
            providers[provider_id] = provider.key.id()
    return provider, providers

def get_from(data, field):
    if field in data:
        return data[field]["value"]
    return None
    
def product_from_runpath_entity(data, providers=None):
    datasource_id = data["id"]

    if SavingsAccount.query(SavingsAccount.datasource_id == datasource_id).get():
        return

    product_data = data["elements"]
    provider, providers = get_provider(product_data["providerName"]["value"], product_data["imageUrl"]["value"], providers)

    product = SavingsAccount()

    product.provider = provider.key

    product.name = get_from(product_data, "name")
    product.datasource = "runpath"
    product.datasource_id = datasource_id
    if get_from(product_data, "isCashIsas"):
        product.isa_type = "cash"

    if "help to buy" in product.name.lower():
        product.isa_type = "help_to_buy"

    """
    Interest Rates
    """

    rates = product_data["rates"]
    balbins = []
    for rate in rates:
        balbin = InterestRateBalanceBin()
        balbin.aer = get_from(rate, "annualInterestAer")
        balbin.balance = AmountConstraint(
            min=get_from(rate, "investmentMin"),
            max=get_from(rate, "investmentMax")
        )
        balbins.append(balbin)
    product.rate_bins = balbins
    
    product.interest_freq = get_from(product_data, "interestPaymentFrequency").lower()
    
    """
    Constraints
    """
    bal_mn, bal_mx = get_from(product_data, "balanceMin"), get_from(product_data, "balanceMax")
    
    product.balance = bal_mn

    op_mn, op_mx = get_from(product_data, "openingDepositMin"), get_from(product_data, "openingDepositMax")

    product.initial_deposit = op_mn

    age_mn, age_mx = get_from(product_data, "ageMin"), get_from(product_data, "ageMax")

    product.age = age_mn

    if get_from(product_data, "existingMembersOnly"):
        product.existing_members_only = True

    conditions = product_data["featuresNotes"]
    product.conditions = "\n".join(note["value"] for note in conditions)
    
    """
    Accessibility
    """
    if get_from(product_data, "accessType") == "Instant":
        product.instant_access = True
    product.term_data_days = get_from(product_data, "termDateDays")
    product.isa_transfer_in = get_from(product_data, "hasIsaTransferIn")
    product.additions_allowed = get_from(product_data, "furtherAdditionsAllowed")
    product.withdrawals_allowed = get_from(product_data, "withdrawalsAllowed")
    product.early_access_allowed = get_from(product_data, "earlyAccessAllowed")

    accessibility = []
    if get_from(product_data, "telephone"):
        accessibility.append("phone")
    if get_from(product_data, "post"):
        accessibility.append("post")
    if get_from(product_data, "online"):
        accessibility.append("online")
    if get_from(product_data, "branch"):
        accessibility.append("branch")
    product.accessibility = accessibility

    restrictions = product_data["restrictionNotes"]
    product.restrictions = "\n".join(note["value"] for note in restrictions)
    product.put()
    
    return providers
