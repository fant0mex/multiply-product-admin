import os
import logging
import webapp2

from multiply_web_lib import get_session_config, JSONRequestHandler, RequestHandler

from google.appengine.api import users
from google.appengine.ext import ndb

import parse

class LoadFromJson(JSONRequestHandler):
    def loadJson(self):
        providers = {}
        json_data = json.load(open(os.path.join('runpath.json'), 'r'))
        products = json_data['entities']
        logging.info("Uploading data from %d new products", len(products))
    
        for counter, product in enumerate(products):
            providers = parse.product_from_runpath_entity(product, providers)
            if counter > 800:
                break
        self.response.out.write("Loaded sucessfully")

class Home(RequestHandler):
    def get(self):
        self.rend('index.html')

def render_routes(route_template, routes):
    return [(route_template.format(route), routeClass) for route, routeClass in routes]

ROUTES = render_routes("{}", [
    (r'/', Home),
    (r'/load', LoadFromJson)
])

app = webapp2.WSGIApplication(
    ROUTES,
    debug=True,
    config=get_session_config()
)

if __name__ == '__main__':
    app.run(debug=True)