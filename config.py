CONSTANTS = {
    "flags" : {
        "easy_access": "Easy Access",
        "peer_to_peer": "Peer to Peer",
        "islamic_law": "Islamic Law",
    },
    "accessibility": {
        "online": "Online",
        "branch": "In Branch",
        "post": "By Post",
        "phone": "By Phone",
    },
    "isa_types": {
        "none": "None",
        "cash": "Cash ISA",
        "lisa": "Lifetime ISA",
        "ss": "Stocks and Shares ISA",
        "help_to_buy": "Help To Buy"
    },
    "interest_freq": {
        "monthly": "Monthly",
        "yearly": "Yearly",
        "anniversary": "Anniversary",
        "maturity": "Maturity"
    }
}

USERS = {
    'email': 'joe@multiply.ai'
}
